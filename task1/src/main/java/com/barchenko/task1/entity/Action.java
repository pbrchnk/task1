package com.barchenko.task1.entity;

public class Action {

    private Cmd cmd;
    private String path;

    public Action() {
    }

    public Cmd getCmd() {
        return cmd;
    }

    public void setCmd(Cmd cmd) {
        this.cmd = cmd;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

        @Override
    public String toString() {
        return "Action{" +
                "cmd='" + cmd + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
