package com.barchenko.task1.service;

import com.barchenko.task1.entity.Action;
import com.barchenko.task1.entity.Cmd;
import com.barchenko.task1.entity.JsonObject;
import com.barchenko.task1.service.impl.DoCompressCommandImpl;
import com.barchenko.task1.service.impl.DoToArrayCommandImpl;
import com.barchenko.task1.service.impl.DoUnwindCommandImpl;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public abstract class Command {

    protected static final Configuration configuration = Configuration.builder()
            .jsonProvider(new JacksonJsonNodeJsonProvider())
            .mappingProvider(new JacksonMappingProvider())
            .build();

    public static Command CommandType(Action action) {
        Cmd cmd = action.getCmd();
        switch (cmd) {
            case TO_ARRAY:
                return new DoToArrayCommandImpl();
            case UNWIND:
                return new DoUnwindCommandImpl();
            case COMPRESS:
                return new DoCompressCommandImpl();
            default:
                throw new IllegalArgumentException("Incorrect Code");
        }
    }

    public ResponseEntity<?> responseEntity(JsonObject myJson){
        return new ResponseEntity(myJson, HttpStatus.OK);
    }

    public abstract JsonObject doCommand(JsonObject jsonObject, Action action);

}
