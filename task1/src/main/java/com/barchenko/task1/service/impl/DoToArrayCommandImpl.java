package com.barchenko.task1.service.impl;

import com.barchenko.task1.entity.Action;
import com.barchenko.task1.entity.JsonObject;
import com.barchenko.task1.service.Command;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;

import java.io.IOException;

public class DoToArrayCommandImpl extends Command {

    @Override
    public JsonObject doCommand(JsonObject jsonObject, Action action) {
        JsonObject result = null;
        try {
            result = toarray(jsonObject, action);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private JsonObject toarray(JsonObject jsonObject, Action action) throws IOException {
        String path = action.getPath();
        ObjectMapper mapper = new ObjectMapper();
        String requestBean = mapper.writeValueAsString(jsonObject);
        Object newValue = JsonPath.read(requestBean, path);
        JsonNode findField = JsonPath.using(configuration)
                .parse(requestBean).set(path, new Object[]{newValue}).json();
        JsonObject convert = mapper.readValue(findField.toString(), JsonObject.class);
        return convert;
    }
}
