package com.barchenko.task1.service.impl;

import com.barchenko.task1.entity.Action;
import com.barchenko.task1.entity.JsonObject;
import com.barchenko.task1.service.Command;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;

import java.io.IOException;

public class DoCompressCommandImpl extends Command {

    @Bean
    public DoCompressCommandImpl doCompressCommandImpl(){
        return new DoCompressCommandImpl();
    }

    @Override
    public JsonObject doCommand(JsonObject jsonObject, Action action) {
        String result = null;
        ObjectMapper mapper = new ObjectMapper();
        JsonObject convert = null;
        try {
            result = compress(jsonObject);
            convert = mapper.readValue(result, JsonObject.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return convert;
    }

    private String compress(JsonObject myJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String requestBean = mapper.writeValueAsString(myJson);

        return requestBean;
    }
}
