package com.barchenko.task1.service.impl;

import com.barchenko.task1.entity.Action;
import com.barchenko.task1.entity.JsonObject;
import com.barchenko.task1.service.Command;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DoUnwindCommandImpl extends Command {

    @Override
    public JsonObject doCommand(JsonObject jsonObject, Action action) {
        JsonObject result = null;
        try {
            result = unwind(jsonObject, action);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    private JsonObject unwind(JsonObject jsonObject, Action action) throws IOException {
        String path = action.getPath();
        ObjectMapper mapper = new ObjectMapper();
        String jsonObjectRequestBean = mapper.writeValueAsString(jsonObject);
        JSONArray sizes = JsonPath.read(jsonObjectRequestBean, path);
        String findTargetObjectPath = path.substring(path.indexOf("$"),path.lastIndexOf("."));
        String findElementToChangePath = path.substring(path.lastIndexOf("."));
        findElementToChangePath = "$"+findElementToChangePath;
        Object findObj = JsonPath.read(jsonObjectRequestBean, findTargetObjectPath);
        String payloadRequestBeanFromFindObject = mapper.writeValueAsString(findObj);
        List<String> list = new ArrayList();
        for (Object s:sizes) {
            JsonNode findField = JsonPath.using(configuration)
                    .parse(payloadRequestBeanFromFindObject).set(findElementToChangePath, s.toString()).json();

            list.add(findField.toString());
        }
        JsonNode payloadElement = mapper.readValue(list.toString(), JsonNode.class);
        JsonNode resultNode = JsonPath.using(configuration)
                .parse(jsonObjectRequestBean).set(findTargetObjectPath, payloadElement).json();
        String resultToString = mapper.writeValueAsString(resultNode);;
        JsonObject resultJsonObject = mapper.readValue(resultToString, JsonObject.class);
        return resultJsonObject;
    }
}
