package com.barchenko.task1.entity;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class JsonObject {
    private List<Action> actions;
    private JsonNode payload;

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public JsonNode getPayload() {
        return payload;
    }

    public void setPayload(JsonNode payload) {
        this.payload = payload;
    }


    @Override
    public String toString() {
        return "MyJson{" +
                "actions=" + actions +
                ", payloads=" + payload +
                '}';
    }

}
