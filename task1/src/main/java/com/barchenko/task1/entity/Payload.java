package com.barchenko.task1.entity;

import java.util.Arrays;
import java.util.List;

public class Payload {
    private Integer id;
    private String item;
    private String[] sizes;

    public Payload() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String[] getSizes() {
        return sizes;
    }

    public void setSizes(String[] sizes) {
        this.sizes = sizes;
    }

    @Override
    public String toString() {
        return "Payload{" +
                "id='" + id + '\'' +
                ", item='" + item + '\'' +
                ", sizes=" + Arrays.toString(sizes) +
                '}';
    }
}
