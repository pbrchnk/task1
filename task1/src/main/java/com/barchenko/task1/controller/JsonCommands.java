package com.barchenko.task1.controller;

import com.barchenko.task1.entity.*;
import com.barchenko.task1.service.Command;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
public class JsonCommands {

    @RequestMapping(path = "/execute", method = RequestMethod.POST)
    public JsonObject saveJSON(@RequestBody JsonObject json) {
        List<Action> actions = json.getActions();
        Command command;
        for (int i = 0; i < actions.size(); i++) {
            Action action = json.getActions().get(i);
            command = Command.CommandType(action);
            json = command.doCommand(json, action);
        }
        return json;
    }

}
