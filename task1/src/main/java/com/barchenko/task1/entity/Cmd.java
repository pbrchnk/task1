package com.barchenko.task1.entity;

public enum Cmd {
    TO_ARRAY, UNWIND, COMPRESS;
}
